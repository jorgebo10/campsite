package com.campsite;

import static com.campsite.domain.campsite.CampsiteId.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.campsite.domain.CampsiteOpenDate;
import com.campsite.domain.CampsiteOpenDateRepository;
import com.campsite.domain.CampsiteOpenDatesService;
import com.campsite.domain.DateRange;
import com.campsite.domain.campsite.CampsiteId;

@Service
@Transactional
public class CampsiteAvailabilityApplicationService {
    private final CampsiteOpenDatesService campsiteOpenDatesService;
    private final CampsiteOpenDateRepository campsiteOpenDateRepository;

    @Autowired
    public CampsiteAvailabilityApplicationService(final CampsiteOpenDatesService campsiteOpenDatesService,
                                                  final CampsiteOpenDateRepository campsiteOpenDateRepository) {
        this.campsiteOpenDatesService = campsiteOpenDatesService;
        this.campsiteOpenDateRepository = campsiteOpenDateRepository;
    }

    public List<CampsiteAvailableOpenDate> findAvailability(final FindCampsiteAvailabilityCommand findCampsiteAvailabilityCommand) {
        final DateRange dateRange = DateRange.of(findCampsiteAvailabilityCommand.getStartDate(), findCampsiteAvailabilityCommand.getEndDate());
        final CampsiteId campsiteId = campsiteIdWith(findCampsiteAvailabilityCommand.getCampsiteId());

        final List<CampsiteOpenDate> allocableDates = campsiteOpenDatesService
                .findAvailableByCampsiteIdAndDateRange(campsiteId, dateRange);

        return allocableDates.stream()
                .map(cop -> new CampsiteAvailableOpenDate(cop.getDate(), cop.getCampsiteId().getId()))
                .collect(Collectors.toList());
    }

    public void lockDates(final TakeCampsiteOpenDateCommand command) {
        final CampsiteId campsiteId = campsiteIdWith(command.getCampsiteId());
        final List<LocalDate> dates = command.getDate();

        for (LocalDate date : dates) {
            final Optional<CampsiteOpenDate> optionalCampsiteOpenDate = campsiteOpenDateRepository
                    .findByCampsiteIdAndDate(campsiteId, date);

            final CampsiteOpenDate campsiteOpenDate = optionalCampsiteOpenDate.orElseThrow(
                    IllegalArgumentException::new);
            campsiteOpenDate.take();
            campsiteOpenDateRepository.save(campsiteOpenDate);
        }
    }

    public void releaseDates(final ReleaseCampsiteOpenDateCommand command) {
        final CampsiteId campsiteId = campsiteIdWith(command.getCampsiteId());
        final List<LocalDate> dates = command.getDate();

        for (LocalDate date : dates) {
            final Optional<CampsiteOpenDate> optionalCampsiteOpenDate = campsiteOpenDateRepository
                    .findByCampsiteIdAndDate(campsiteId, date);

            final CampsiteOpenDate campsiteOpenDate = optionalCampsiteOpenDate.orElseThrow(
                    IllegalArgumentException::new);
            campsiteOpenDate.release();
            campsiteOpenDateRepository.save(campsiteOpenDate);
        }
    }
}
