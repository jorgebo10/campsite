package com.campsite;

import java.time.LocalDate;

public class FindCampsiteAvailabilityCommand {
    private final long campsiteId;
    private final LocalDate startDate;
    private final LocalDate endDate;

    public FindCampsiteAvailabilityCommand(long campsiteId, LocalDate startDate, LocalDate endDate) {
        this.campsiteId = campsiteId;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public long getCampsiteId() {
        return campsiteId;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public LocalDate getStartDate() {

        return startDate;
    }
}
