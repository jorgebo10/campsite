package com.campsite.interfaces;

import com.campsite.CampsiteAvailabilityApplicationService;
import com.campsite.CampsiteAvailableOpenDate;
import com.campsite.FindCampsiteAvailabilityCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
public class CampsiteAvailabilityController {
    private final CampsiteAvailabilityApplicationService campsiteAvailabilityApplicationService;

    @Autowired
    public CampsiteAvailabilityController(final CampsiteAvailabilityApplicationService campsiteAvailabilityApplicationService) {
        this.campsiteAvailabilityApplicationService = campsiteAvailabilityApplicationService;
    }

    @GetMapping(value = "/campsite/{campsiteId}/availableOpenDates")
    public List<CampsiteAvailableOpenDate> findAvailability(@PathVariable final Long campsiteId,
                                                            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate startDate,
                                                            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate endDate) {
        final FindCampsiteAvailabilityCommand command = new FindCampsiteAvailabilityCommand(campsiteId, startDate, endDate);
        return campsiteAvailabilityApplicationService.findAvailability(command);
    }
}
