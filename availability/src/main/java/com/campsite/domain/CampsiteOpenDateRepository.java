package com.campsite.domain;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.campsite.domain.campsite.CampsiteId;

@Repository
public interface CampsiteOpenDateRepository extends CrudRepository<CampsiteOpenDate, CampsiteId> {
    List<CampsiteOpenDate> findByCampsiteIdAndDateBetween(final CampsiteId campsiteId, final LocalDate startDate, final LocalDate endDate);

    Optional<CampsiteOpenDate> findByCampsiteIdAndDate(CampsiteId campsiteId, LocalDate date);
}
