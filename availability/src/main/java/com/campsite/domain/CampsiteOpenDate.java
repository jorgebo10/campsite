package com.campsite.domain;

import com.campsite.domain.campsite.CampsiteId;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class CampsiteOpenDate {
    @EmbeddedId
    private CampsiteOpenDateId campsiteOpenDateId;
    private LocalDate date;
    @Embedded
    private CampsiteId campsiteId;
    @Version
    @Column(name = "version")
    private Integer version;
    private boolean available;

    public CampsiteOpenDate() {
        //required by hibernate
    }

    private CampsiteOpenDate(CampsiteOpenDateId campsiteOpenDateId, LocalDate date, CampsiteId campsiteId, boolean available) {
        this.campsiteOpenDateId = campsiteOpenDateId;
        this.date = date;
        this.campsiteId = campsiteId;
        this.available = available;
    }

    public static CampsiteOpenDate newAvailableCampsiteOpenDate(CampsiteOpenDateId campsiteOpenDateId, LocalDate localDate, CampsiteId campsiteId) {
        return new CampsiteOpenDate(campsiteOpenDateId, localDate, campsiteId, false);
    }

    public static CampsiteOpenDate newTakenCampsiteOpenDate(CampsiteOpenDateId campsiteOpenDateId, LocalDate localDate, CampsiteId campsiteId) {
        return new CampsiteOpenDate(campsiteOpenDateId, localDate, campsiteId, true);
    }

    public CampsiteOpenDate take() {
        this.available = false;
        return this;
    }

    public CampsiteOpenDate release() {
        this.available = true;
        return this;

    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public CampsiteOpenDateId getCampsiteOpenDateId() {
        return campsiteOpenDateId;
    }

    public LocalDate getDate() {
        return date;
    }

    public CampsiteId getCampsiteId() {
        return campsiteId;
    }

    public boolean isAvailable() {
        return available;
    }

    public boolean isNotAvaiblable() {
        return !isAvailable();
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CalendarEntry{");
        sb.append("calendarEntryId=").append(campsiteOpenDateId);
        sb.append(", date=").append(date);
        sb.append(", campsiteId=").append(campsiteId);
        sb.append(", available=").append(available);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampsiteOpenDate that = (CampsiteOpenDate) o;
        return Objects.equals(campsiteOpenDateId, that.campsiteOpenDateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(campsiteOpenDateId);
    }
}
