package com.campsite.domain;

import com.campsite.domain.campsite.CampsiteId;

import java.util.List;

public interface CampsiteOpenDatesService {

    List<CampsiteOpenDate> findAvailableByCampsiteIdAndDateRange(final CampsiteId campsiteId, final DateRange dateRange);
}
