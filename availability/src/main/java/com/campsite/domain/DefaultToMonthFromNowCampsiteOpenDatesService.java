package com.campsite.domain;

import com.campsite.domain.campsite.CampsiteId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class DefaultToMonthFromNowCampsiteOpenDatesService implements CampsiteOpenDatesService {
    private final CampsiteOpenDateRepository campsiteOpenDateRepository;

    @Autowired
    public DefaultToMonthFromNowCampsiteOpenDatesService(final CampsiteOpenDateRepository campsiteOpenDateRepository) {
        this.campsiteOpenDateRepository = campsiteOpenDateRepository;
    }

    @Override
    public List<CampsiteOpenDate> findAvailableByCampsiteIdAndDateRange(final CampsiteId campsiteId, final DateRange dateRange) {
        if (dateRange != null) {
            return campsiteOpenDateRepository.findByCampsiteIdAndDateBetween(campsiteId, dateRange.getStartDate(), dateRange.getEndDate());
        }

        final LocalDate now = LocalDate.now();
        return campsiteOpenDateRepository.findByCampsiteIdAndDateBetween(campsiteId, now, now.plusMonths(1));
    }
}
