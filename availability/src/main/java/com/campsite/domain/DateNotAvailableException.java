package com.campsite.domain;

public class DateNotAvailableException extends IllegalStateException {

    public DateNotAvailableException(String s) {
        super(s);
    }
}
