package com.campsite.domain;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Embeddable
public class CampsiteOpenDateId implements Serializable {
    @Column(name = "campsiteOpenDate_id")
    @GeneratedValue(strategy= GenerationType.SEQUENCE)
    private long id;

    public CampsiteOpenDateId() {
        //for hibernate only
    }

    public CampsiteOpenDateId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    //for hibernate only
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CalendarEntryId{");
        sb.append("id=").append(id);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampsiteOpenDateId that = (CampsiteOpenDateId) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
