package com.campsite;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class TakeCampsiteOpenDateCommand {
    private List<LocalDate> dates;
    private long campsiteId;

    public TakeCampsiteOpenDateCommand(List<LocalDate> dates, long campsiteId) {
        this.dates = dates;
        this.campsiteId = campsiteId;
    }

    public List<LocalDate> getDate() {
        return dates;
    }

    public long getCampsiteId() {
        return campsiteId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AllocateCampsiteOpenDateCommand{");
        sb.append("dates=").append(dates);
        sb.append(", campsiteId=").append(campsiteId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TakeCampsiteOpenDateCommand that = (TakeCampsiteOpenDateCommand) o;
        return campsiteId == that.campsiteId &&
                Objects.equals(dates, that.dates);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dates, campsiteId);
    }
}
