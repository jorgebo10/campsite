package com.campsite;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

public class ReleaseCampsiteOpenDateCommand {
    private List<LocalDate> localDate;
    private long campsiteId;

    public ReleaseCampsiteOpenDateCommand(List<LocalDate> localDate, long campsiteId) {
        this.localDate = localDate;
        this.campsiteId = campsiteId;
    }

    public List<LocalDate> getDate() {
        return localDate;
    }

    public long getCampsiteId() {
        return campsiteId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AllocateCampsiteOpenDateCommand{");
        sb.append("localDate=").append(localDate);
        sb.append(", campsiteId=").append(campsiteId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReleaseCampsiteOpenDateCommand that = (ReleaseCampsiteOpenDateCommand) o;
        return campsiteId == that.campsiteId &&
                Objects.equals(localDate, that.localDate);
    }

    @Override
    public int hashCode() {

        return Objects.hash(localDate, campsiteId);
    }
}
