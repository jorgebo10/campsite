package com.campsite;

import jdk.nashorn.internal.ir.annotations.Immutable;

import java.time.LocalDate;
import java.util.Objects;

@Immutable
public final class CampsiteAvailableOpenDate {
    private LocalDate localDate;
    private long campsiteId;

    private CampsiteAvailableOpenDate() {
        //immutable
    }

    public CampsiteAvailableOpenDate(LocalDate localDate, long campsiteId) {
        this.localDate = localDate;
        this.campsiteId = campsiteId;
    }

    public LocalDate getLocalDate() {
        return localDate;
    }

    public long getCampsiteOpenDateId() {
        return campsiteId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CampsiteAllocableDate{");
        sb.append("localDate=").append(localDate);
        sb.append(", campsiteId=").append(campsiteId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampsiteAvailableOpenDate that = (CampsiteAvailableOpenDate) o;
        return Objects.equals(localDate, that.localDate) &&
                Objects.equals(campsiteId, that.campsiteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(localDate, campsiteId);
    }
}


