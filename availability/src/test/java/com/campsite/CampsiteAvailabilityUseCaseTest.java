package com.campsite;

import com.campsite.domain.CampsiteOpenDate;
import com.campsite.domain.CampsiteOpenDateId;
import com.campsite.domain.CampsiteOpenDatesService;
import com.campsite.domain.DateRange;
import com.campsite.domain.campsite.CampsiteId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

import static com.campsite.domain.CampsiteOpenDate.newAvailableCampsiteOpenDate;
import static java.time.LocalDate.now;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CampsiteAvailabilityUseCaseTest {
    @InjectMocks
    private CampsiteAvailabilityApplicationService campsiteAvailabilityUseCase;
    @Mock
    private CampsiteOpenDatesService campsiteOpenDatesService;

    @Test
    public void shouldReturnCampsiteAvailableOpenDates() {
        final LocalDate startDate = now();
        final LocalDate endDate = startDate.plusMonths(3);
        final CampsiteOpenDateId campsiteOpenDateId = new CampsiteOpenDateId(1);

        final CampsiteId campsiteId = CampsiteId.campsiteIdWith(1);
        when(campsiteOpenDatesService.findAvailableByCampsiteIdAndDateRange(
                campsiteId, DateRange.of(startDate, endDate)))
                .thenReturn(getCampsiteOpenDates(campsiteOpenDateId, DateRange.of(startDate, endDate), campsiteId));

        final List<CampsiteAvailableOpenDate> availableDateList = campsiteAvailabilityUseCase.findAvailability(
                new FindCampsiteAvailabilityCommand(campsiteId.getId(), startDate, endDate));

        final CampsiteAvailableOpenDate expected = new CampsiteAvailableOpenDate(startDate, campsiteId.getId());
        assertEquals(expected, availableDateList.get(0));
    }

    private List<CampsiteOpenDate> getCampsiteOpenDates(CampsiteOpenDateId campsiteOpenDateId, DateRange dateRange, CampsiteId campsiteId) {
        return Collections.singletonList(
                newAvailableCampsiteOpenDate(campsiteOpenDateId, dateRange.getStartDate(), campsiteId));
    }
}