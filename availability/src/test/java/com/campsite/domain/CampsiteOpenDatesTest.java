package com.campsite.domain;

import com.campsite.domain.campsite.CampsiteId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.campsite.domain.CampsiteOpenDate.newAvailableCampsiteOpenDate;
import static com.campsite.domain.CampsiteOpenDate.newTakenCampsiteOpenDate;
import static java.time.LocalDate.now;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CampsiteOpenDatesTest {
    @Mock
    private CampsiteOpenDateRepository campsiteOpenDateRepository;
    @InjectMocks
    private DefaultToMonthFromNowCampsiteOpenDatesService campsiteOpenDatesService;

    @Test
    public void shouldReturnOneMonthFromNowCalendarEntriesWhenDateRangeNotSpecified() {
        final LocalDate now = LocalDate.now();
        final List<CampsiteOpenDate> expectedDays = getCampsiteDates(now, now.plusMonths(1));
        final CampsiteId campsiteId = CampsiteId.campsiteIdWith(1);
        final DateRange dateRange = DateRange.of(now, now.plusMonths(1));

        when(campsiteOpenDateRepository.findByCampsiteIdAndDateBetween(campsiteId, dateRange.getStartDate(), dateRange.getEndDate()))
                .thenReturn(findByCampsiteIdAndDateRange(campsiteId, dateRange));

        final List<CampsiteOpenDate> availableDays = campsiteOpenDatesService.findAvailableByCampsiteIdAndDateRange(campsiteId, null);
        assertEquals(expectedDays, availableDays);
    }

    @Test
    public void shouldReturnInDateRangeCalendarEntriesWhenDateRangeIsSpecified() {
        final LocalDate startDate = LocalDate.of(2018, 1, 1);
        final LocalDate endDate = LocalDate.of(2018, 3, 4);
        final List<CampsiteOpenDate> expectedDays = getCampsiteDates(startDate, endDate);

        final CampsiteId campsiteId = CampsiteId.campsiteIdWith(1);
        final DateRange dateRange = DateRange.of(startDate, endDate);

        when(campsiteOpenDateRepository.findByCampsiteIdAndDateBetween(campsiteId, dateRange.getStartDate(), dateRange.getEndDate()))
                .thenReturn(findByCampsiteIdAndDateRange(campsiteId, dateRange));

        final List<CampsiteOpenDate> availableDays = campsiteOpenDatesService.findAvailableByCampsiteIdAndDateRange(
                campsiteId,
                dateRange);
        assertEquals(expectedDays, availableDays);
    }

    private List<CampsiteOpenDate> getCampsiteDates(final LocalDate startDate, final LocalDate endDate) {
        return asList(
                newAvailableCampsiteOpenDate(new CampsiteOpenDateId(1), startDate, CampsiteId.campsiteIdWith(1)),
                newAvailableCampsiteOpenDate(new CampsiteOpenDateId(2), endDate, CampsiteId.campsiteIdWith(1))
        );
    }

    private List<CampsiteOpenDate> findByCampsiteIdAndDateRange(final CampsiteId campsiteId,
                                                                final DateRange dateRange) {
        if (dateRange.getStartDate().equals(now()) && dateRange.getEndDate().equals(now().plusMonths(1))) {
            final LocalDate now = now();
            return Stream.of(
                    newAvailableCampsiteOpenDate(new CampsiteOpenDateId(1), now, campsiteId),
                    newAvailableCampsiteOpenDate(new CampsiteOpenDateId(2), now.plusMonths(1), campsiteId),
                    newTakenCampsiteOpenDate(new CampsiteOpenDateId(3), now.plusDays(1), campsiteId)
            )
                    .filter(CampsiteOpenDate::isNotAvaiblable)
                    .collect(Collectors.toList());
        }

        final LocalDate startDate = dateRange.getStartDate();
        final LocalDate endDate = dateRange.getEndDate();
        return Stream.of(
                newAvailableCampsiteOpenDate(new CampsiteOpenDateId(1), startDate, CampsiteId.campsiteIdWith(1)),
                newAvailableCampsiteOpenDate(new CampsiteOpenDateId(2), endDate, CampsiteId.campsiteIdWith(1)),
                newTakenCampsiteOpenDate(new CampsiteOpenDateId(3), endDate.minusDays(2), CampsiteId.campsiteIdWith(1))
        )
                .filter(CampsiteOpenDate::isNotAvaiblable)
                .collect(Collectors.toList());
    }
}
