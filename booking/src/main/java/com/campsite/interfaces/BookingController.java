package com.campsite.interfaces;

import java.time.LocalDate;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.campsite.application.NewBookingCommand;
import com.campsite.application.sagas.BookingDto;
import com.campsite.application.sagas.NewBookingSaga;

@RestController
public class BookingController {
    private final NewBookingSaga newBookingSaga;

    @Autowired
    public BookingController(final NewBookingSaga newBookingSaga) {
        this.newBookingSaga = newBookingSaga;
    }

    @PostMapping("booking")
    public Long book(@RequestBody final @Valid BookingDto bookingDto) {
        final NewBookingCommand command = new NewBookingCommand(LocalDate.now(), bookingDto.getCampsiteId(), bookingDto.getWantToBookDates(), bookingDto.getGuests());
        return newBookingSaga.newBooking(command);
    }
}
