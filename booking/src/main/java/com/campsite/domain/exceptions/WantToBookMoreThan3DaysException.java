package com.campsite.domain.exceptions;

public class WantToBookMoreThan3DaysException extends IllegalArgumentException {

    public WantToBookMoreThan3DaysException(String s) {
        super(s);
    }
}
