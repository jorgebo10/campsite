package com.campsite.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BookingDateIsNotBetween1And30DaysAheadOfArrivalDateException extends IllegalArgumentException {

    public BookingDateIsNotBetween1And30DaysAheadOfArrivalDateException(String s) {
        super(s);
    }
}
