package com.campsite.domain.booking.dates;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Embeddable
public final class BookingDateId implements Serializable {
    @Column(name = "bookingDate_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    public BookingDateId() {
        //required by hibernate
    }

    private BookingDateId(long id) {
        //immutable class
    }

    public static BookingDateId of(int id) {
        return new BookingDateId(id);
    }

    //required by hibenrate
    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BookingDateId{");
        sb.append("id=").append(id);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookingDateId that = (BookingDateId) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
