package com.campsite.domain.campsite;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import jdk.nashorn.internal.ir.annotations.Immutable;

@Embeddable
public final class CampsiteId implements Serializable {
    @Column(name = "campsite_id")
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    public CampsiteId() {
        //Required by hibernate
    }

    private CampsiteId(long id) {
        this.id = id;
    }

    public static CampsiteId campsiteIdWith(long campsiteId) {
        return new CampsiteId(campsiteId);
    }

    public long getId() {
        return id;
    }

    //Required by hibernate
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CampsiteId{");
        sb.append("id=").append(id);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CampsiteId that = (CampsiteId) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
