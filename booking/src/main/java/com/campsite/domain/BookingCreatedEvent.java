package com.campsite.domain;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.campsite.domain.dates.BookedDate;
import com.campsite.domain.campsite.CampsiteId;
import jdk.nashorn.internal.ir.annotations.Immutable;

@Immutable
public final class BookingCreatedEvent {
    private LocalDate occurredOn;
    private List<LocalDate> dates;
    private CampsiteId campsiteId;

    private BookingCreatedEvent() {
        //immutable
    }

    public BookingCreatedEvent(final CampsiteId campsiteId, final List<LocalDate> dates) {
        this.campsiteId = campsiteId;
        this.dates = dates;
        this.occurredOn = LocalDate.now();
    }

    public LocalDate getOccurredOn() {
        return occurredOn;
    }

    public List<LocalDate> getDates() {
        return Collections.unmodifiableList(this.dates);
    }

    public CampsiteId getCampsiteId() {
        return campsiteId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DateBookedEvent{");
        sb.append("occurredOn=").append(occurredOn);
        sb.append(", dates=").append(dates);
        sb.append(", campsiteId=").append(campsiteId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookingCreatedEvent that = (BookingCreatedEvent) o;
        return Objects.equals(occurredOn, that.occurredOn) &&
                Objects.equals(dates, that.dates) &&
                Objects.equals(campsiteId, that.campsiteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(occurredOn, dates, campsiteId);
    }

    public static BookingCreatedEvent from(List<BookedDate> bookedDates, CampsiteId campsiteId) {
        final List<LocalDate> localDates = bookedDates.stream()
                .map(BookedDate::getDate)
                .collect(Collectors.toList());
        return new BookingCreatedEvent(campsiteId, localDates);
    }
}
