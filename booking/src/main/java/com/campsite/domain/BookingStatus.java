package com.campsite.domain;

public enum BookingStatus {
    PENDING,
    CREATED,
    CANCELLED
}
