package com.campsite.domain;

import java.util.Objects;
import javax.persistence.Embeddable;

@Embeddable
public final class Guest {
    private String fullname;

    public Guest() {
        //Required by hibernate
    }

    public Guest(String fullname) {
        this.fullname = fullname;
    }

    public String getFullname() {
        return fullname;
    }

    //Required by hibernate
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Guest{");
        sb.append("fullname='").append(fullname).append('\'');
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guest guest = (Guest) o;
        return Objects.equals(fullname, guest.fullname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fullname);
    }
}
