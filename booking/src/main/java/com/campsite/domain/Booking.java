package com.campsite.domain;

import com.campsite.domain.campsite.CampsiteId;
import com.campsite.domain.dates.BookedDate;
import com.campsite.domain.exceptions.BookingDateIsNotBetween1And30DaysAheadOfArrivalDateException;
import com.campsite.domain.exceptions.WantToBookDatesAreNotConsecutiveException;
import com.campsite.domain.exceptions.WantToBookMoreThan3DaysException;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Entity
public class Booking {
    private static final int MAX_RESERVATION_DAYS = 3;
    private static final int MAX_BOOKING_DAYS_AHEAD = 30;
    @EmbeddedId
    private BookingId bookingId;
    @Embedded
    private CampsiteId campsiteId;
    private LocalDateTime timeStamp;
    @ElementCollection
    @CollectionTable(name = "booking_guests", joinColumns = @JoinColumn(name = "booking_id"),
            foreignKey = @ForeignKey(name = "booking_guests_guests_fk"))
    private List<Guest> guests;
    @ElementCollection
    @CollectionTable(name = "booking_bookedDates", joinColumns = @JoinColumn(name = "booking_id"),
            foreignKey = @ForeignKey(name = "booking_booked_dates_fk"))
    private List<BookedDate> bookedDates;
    private BookingStatus bookingStatus;
    @Column(name = "version")
    private Integer version;

    public BookingStatus getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    public Booking newBookedDates(final LocalDate bookingDate, final List<LocalDate> wantToBookDates) {
        boolean isSearchDateOneMonthAheadThanArrivalDate = isBookingDateBetween30And1DayAheadThanArrivalDate(bookingDate, wantToBookDates.get(0));
        if (!isSearchDateOneMonthAheadThanArrivalDate) {
            throw new BookingDateIsNotBetween1And30DaysAheadOfArrivalDateException(format("Booking date not between 1 and 30 days of arrival. BookingDate: %s ArrivalDate: %s", bookingDate, wantToBookDates.get(0)));
        }

        boolean areBookingDatesNoMoreThanThree = areWantToBookDatesNoMoreThanThree(wantToBookDates);
        if (!areBookingDatesNoMoreThanThree) {
            throw new WantToBookMoreThan3DaysException(format("Want to book more than three days: %s", wantToBookDates.size()));
        }

        boolean areConsecutiveDates = areWantToBookDatesConsecutive(wantToBookDates);
        if (!areConsecutiveDates) {
            throw new WantToBookDatesAreNotConsecutiveException(format("Not consecutives booking dates: %s", wantToBookDates.toString()));
        }


        this.bookedDates = wantToBookDates.stream()
                .map(BookedDate::new)
                .collect(Collectors.toList());
        return this;
    }

    public boolean areWantToBookDatesNoMoreThanThree(final List<LocalDate> wantToBookDates) {
        return wantToBookDates.size() <= MAX_RESERVATION_DAYS;
    }

    public boolean isBookingDateBetween30And1DayAheadThanArrivalDate(final LocalDate bookingDate, final LocalDate arrivalDate) {
        return bookingDate.isBefore(arrivalDate) && bookingDate.isAfter(arrivalDate.minusDays(MAX_BOOKING_DAYS_AHEAD));
    }

    public boolean areWantToBookDatesConsecutive(final List<LocalDate> wantToBookDates) {
        final LocalDate firstDate = wantToBookDates.get(0);
        int datePosition = wantToBookDates.size() - 1;

        while (datePosition > 0) {
            if (!wantToBookDates.get(datePosition).equals(firstDate.plusDays(datePosition))) {
                return false;
            }
            datePosition--;
        }
        return true;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public void setCampsiteId(CampsiteId campsiteId) {
        this.campsiteId = campsiteId;
    }

    public CampsiteId getCampsiteId() {
        return campsiteId;
    }

    public BookingId getBookingId() {
        return bookingId;
    }

    public void setBookingId(BookingId bookingId) {
        this.bookingId = bookingId;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    public List<Guest> getGuests() {
        return guests;
    }

    public void setGuests(List<Guest> guests) {
        this.guests = Collections.unmodifiableList(guests);
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Booking booking = (Booking) o;
        return Objects.equals(bookingId, booking.bookingId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingId);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Booking{");
        sb.append("bookingId=").append(bookingId);
        sb.append(", timeStamp=").append(timeStamp);
        sb.append(", guests=").append(guests);
        sb.append('}');
        return sb.toString();
    }

    public List<BookedDate> getBookedDates() {
        return bookedDates;
    }
}
