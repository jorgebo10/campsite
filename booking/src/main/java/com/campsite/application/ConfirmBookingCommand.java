package com.campsite.application;

import java.util.Objects;

public class ConfirmBookingCommand {
    private long bookingId;

    public ConfirmBookingCommand(long bookingId) {
        this.bookingId = bookingId;
    }

    public long getBookingId() {
        return bookingId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("CancelBookingCommand{");
        sb.append("bookingId=").append(bookingId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ConfirmBookingCommand that = (ConfirmBookingCommand) o;
        return bookingId == that.bookingId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(bookingId);
    }
}
