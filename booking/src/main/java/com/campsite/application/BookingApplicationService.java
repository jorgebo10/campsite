package com.campsite.application;

import static com.campsite.domain.BookingId.*;
import static com.campsite.domain.campsite.CampsiteId.*;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.campsite.domain.Booking;
import com.campsite.domain.BookingNotFoundException;
import com.campsite.domain.BookingRepository;
import com.campsite.domain.BookingStatus;
import com.campsite.domain.Guest;

@Service
@Transactional
public class BookingApplicationService {
    private final BookingRepository bookingRepository;

    @Autowired
    public BookingApplicationService(final BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }

    public long newBooking(final NewBookingCommand newBookingCommand) {
        final Booking booking = new Booking();
        booking.setBookingId(withBookingId(UUID.randomUUID().getMostSignificantBits() & Long.MAX_VALUE));
        booking.setBookingStatus(BookingStatus.PENDING);
        booking.setGuests(newBookingCommand.getGuests().stream()
                .map(Guest::new)
                .collect(Collectors.toList()));
        booking.setCampsiteId(campsiteIdWith(newBookingCommand.getCampsiteId()));
        booking.newBookedDates(newBookingCommand.getBookingDate(), newBookingCommand.getWantToBookDates());

        final Booking updatedBooking = bookingRepository.save(booking);

        return updatedBooking.getBookingId().getId();
    }

    public void confirmBooking(final ConfirmBookingCommand confirmBookingCommand) {
        Optional<Booking> bookingOptional = this.bookingRepository.findById(withBookingId(confirmBookingCommand.getBookingId()));
        Booking booking =  bookingOptional.orElseThrow(BookingNotFoundException::new);
        booking.setBookingStatus(BookingStatus.CREATED);
        bookingRepository.save(booking);
    }

    public void cancelBooking(final CancelBookingCommand cancelBookingCommand) {
        Optional<Booking> bookingOptional = this.bookingRepository.findById(withBookingId(cancelBookingCommand.getBookingId()));
        Booking booking =  bookingOptional.orElseThrow(BookingNotFoundException::new);
        if (booking.getBookingStatus() != BookingStatus.CREATED) {
            throw new IllegalStateException("Booking is not in created state");
        }
        booking.setBookingStatus(BookingStatus.CANCELLED);
        bookingRepository.save(booking);
    }

    public Booking getBookingById(GetBookingCommand getBookingCommand) {
        Optional<Booking> bookingOptional = this.bookingRepository.findById(withBookingId(getBookingCommand.getBookingId()));
        return bookingOptional.orElseThrow(BookingNotFoundException::new);
    }
}
