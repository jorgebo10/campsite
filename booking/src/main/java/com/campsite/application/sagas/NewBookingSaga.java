package com.campsite.application.sagas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.campsite.CampsiteAvailabilityApplicationService;
import com.campsite.TakeCampsiteOpenDateCommand;
import com.campsite.application.BookingApplicationService;
import com.campsite.application.CancelBookingCommand;
import com.campsite.application.ConfirmBookingCommand;
import com.campsite.application.NewBookingCommand;

@Service
public class NewBookingSaga {
    private final BookingApplicationService bookingApplicationService;
    private final CampsiteAvailabilityApplicationService campsiteAvailabilityApplicationService;

    @Autowired
    public NewBookingSaga(final BookingApplicationService bookingApplicationService,
                          final CampsiteAvailabilityApplicationService campsiteAvailabilityApplicationService) {
        this.bookingApplicationService = bookingApplicationService;
        this.campsiteAvailabilityApplicationService = campsiteAvailabilityApplicationService;
    }

    public long newBooking(final NewBookingCommand newBookingCommand) {
        final long bookingId = bookingApplicationService.newBooking(newBookingCommand);

        try {
            campsiteAvailabilityApplicationService.lockDates(new TakeCampsiteOpenDateCommand(newBookingCommand.getWantToBookDates(), newBookingCommand.getCampsiteId()));
            bookingApplicationService.confirmBooking(new ConfirmBookingCommand(bookingId));
            return bookingId;
        } catch (Exception e) {
            bookingApplicationService.cancelBooking(new CancelBookingCommand(bookingId));
            throw e;
        }
    }
}
