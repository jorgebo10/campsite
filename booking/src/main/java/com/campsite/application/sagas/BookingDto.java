package com.campsite.application.sagas;

import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.Size;

public class BookingDto {
    @Size(min = 1, max = 3)
    private List<LocalDate> wantToBookDates;
    @Size(min = 1)
    private List<String> guests;

    private long campsiteId;

    public List<LocalDate> getWantToBookDates() {
        return wantToBookDates;
    }

    public void setWantToBookDates(List<LocalDate> wantToBookDates) {
        this.wantToBookDates = wantToBookDates;
    }

    public List<String> getGuests() {
        return guests;
    }

    public void setGuests(List<String> guests) {
        this.guests = guests;
    }

    public long getCampsiteId() {
        return campsiteId;
    }

    public void setCampsiteId(long campsiteId) {
        this.campsiteId = campsiteId;
    }
}
