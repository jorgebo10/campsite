package com.campsite.application.sagas;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.campsite.CampsiteAvailabilityApplicationService;
import com.campsite.ReleaseCampsiteOpenDateCommand;
import com.campsite.application.BookingApplicationService;
import com.campsite.application.CancelBookingCommand;
import com.campsite.application.GetBookingCommand;
import com.campsite.domain.Booking;
import com.campsite.domain.dates.BookedDate;

@Service
public class CancelBookingSaga {
    private final BookingApplicationService bookingApplicationService;
    private final CampsiteAvailabilityApplicationService campsiteAvailabilityApplicationService;

    @Autowired
    public CancelBookingSaga(final BookingApplicationService bookingApplicationService,
                             final CampsiteAvailabilityApplicationService campsiteAvailabilityApplicationService) {
        this.bookingApplicationService = bookingApplicationService;
        this.campsiteAvailabilityApplicationService = campsiteAvailabilityApplicationService;
    }

    public void cancelBooking(final CancelBookingCommand cancelBookingCommand) {
        final Booking booking = bookingApplicationService.getBookingById(new GetBookingCommand(cancelBookingCommand.getBookingId()));

        final List<LocalDate> releaseDates = booking.getBookedDates().stream()
                .map(BookedDate::getDate)
                .collect(Collectors.toList());
        campsiteAvailabilityApplicationService.releaseDates(new ReleaseCampsiteOpenDateCommand(releaseDates, booking.getCampsiteId().getId()));
        bookingApplicationService.cancelBooking(new CancelBookingCommand(cancelBookingCommand.getBookingId()));
    }
}
