package com.campsite.application;

import java.util.Objects;

public class GetBookingCommand {
    private long bookingId;

    public GetBookingCommand(long bookingId) {
        this.bookingId = bookingId;
    }

    public long getBookingId() {
        return bookingId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("GetBookingCommand{");
        sb.append("bookingId=").append(bookingId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetBookingCommand that = (GetBookingCommand) o;
        return bookingId == that.bookingId;
    }

    @Override
    public int hashCode() {

        return Objects.hash(bookingId);
    }
}
