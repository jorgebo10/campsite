package com.campsite.application;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;

import jdk.nashorn.internal.ir.annotations.Immutable;

@Immutable
public final class NewBookingCommand {
    private LocalDate bookingDate;
    private List<LocalDate> wantToBookDates;
    private List<String> guests;
    private long campsiteId;

    private NewBookingCommand() {
        //immutable
    }

    public NewBookingCommand(final LocalDate bookingDate, final long campsiteId, final List<LocalDate> wantToBookDates, final List<String> guests) {
        this.bookingDate = bookingDate;
        this.campsiteId = campsiteId;
        this.wantToBookDates = wantToBookDates;
        this.guests = guests;
    }

    public long getCampsiteId() {
        return campsiteId;
    }

    public LocalDate getBookingDate() {
        return bookingDate;
    }

    public List<LocalDate> getWantToBookDates() {
        return wantToBookDates;
    }

    public List<String> getGuests() {
        return guests;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("NewBookingCommand{");
        sb.append("bookingDate=").append(bookingDate);
        sb.append(", wantToBookDates=").append(wantToBookDates);
        sb.append(", guests=").append(guests);
        sb.append(", campsiteId=").append(campsiteId);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NewBookingCommand that = (NewBookingCommand) o;
        return Objects.equals(bookingDate, that.bookingDate) &&
                Objects.equals(wantToBookDates, that.wantToBookDates) &&
                Objects.equals(guests, that.guests) &&
                Objects.equals(campsiteId, that.campsiteId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookingDate, wantToBookDates, guests, campsiteId);
    }
}
