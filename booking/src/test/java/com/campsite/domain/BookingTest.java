package com.campsite.domain;

import static java.util.Arrays.*;
import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;

public class BookingTest {

    private static final long MIN_BOOKING_DAYS_AHEAD = 1;

    private final Booking booking = new Booking();

    @Test
    public void givenWantToBookDatesAreFourWhenBookingThenBookingIsNotValid() {
        LocalDate now = LocalDate.now();
        assertFalse(booking.areWantToBookDatesNoMoreThanThree(asList(now, now.plusDays(1), now.plusDays(2), now.plusDays(3))));
    }

    @Test
    public void givenWantToBookDatesAreThreeWhenBookingThenBookingIsValid() {
        LocalDate now = LocalDate.now();
        assertTrue(booking.areWantToBookDatesNoMoreThanThree(
                asList(now, now.plusDays(1), now.plusDays(2))));
    }

    @Test
    public void givenWantToBookDatesAreConsecutiveWhenBookingThenBookingIsValid() {
        LocalDate now = LocalDate.now();
        assertTrue(booking.areWantToBookDatesConsecutive(
                asList(now, now.plusDays(1), now.plusDays(2), now.plusDays(3))));
    }

    @Test
    public void givenWantToBookDatesAreNotConsecutiveWhenBookingThenBookingIsNotValid() {
        LocalDate now = LocalDate.now();
        assertFalse(booking.areWantToBookDatesConsecutive(
                asList(now, now.plusDays(1), now.plusDays(3), now.plusDays(5))));
    }

    @Test
    public void givenBookingDateIs30DaysAheadBeforeArrivalDateWhenBookingThenItIsValid() {
        final LocalDate arrivalDate = LocalDate.now();
        assertTrue(booking.isBookingDateBetween30And1DayAheadThanArrivalDate(arrivalDate.minusDays(MIN_BOOKING_DAYS_AHEAD), arrivalDate));
    }

    @Test
    public void givenBookingDateIs10DaysAheadBeforeArrivalDateWhenBookingThenItIsValid() {
        final LocalDate arrivalDate = LocalDate.now();
        assertTrue(booking.isBookingDateBetween30And1DayAheadThanArrivalDate(arrivalDate.minusDays(10), arrivalDate));
    }

    @Test
    public void givenBookingDateIs31DaysAheadBeforeArrivalDateWhenBookingThenItIsNotValid() {
        final LocalDate arrivalDate = LocalDate.now();
        assertFalse(booking.isBookingDateBetween30And1DayAheadThanArrivalDate(arrivalDate.minusDays(31), arrivalDate));
    }

    @Test
    public void givenBookingDateIsSameDateAsArrivalDateWhenBookingThenItIsNotValid() {
        final LocalDate arrivalDate = LocalDate.now();
        assertFalse(booking.isBookingDateBetween30And1DayAheadThanArrivalDate(arrivalDate, arrivalDate));
    }
}
