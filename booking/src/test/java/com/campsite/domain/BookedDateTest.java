package com.campsite.domain;

import static java.time.LocalDate.*;
import static java.util.Arrays.*;
import static org.junit.Assert.*;

import java.time.LocalDate;
import java.util.List;

import com.campsite.domain.exceptions.WantToBookMoreThan3DaysException;
import org.junit.Before;
import org.junit.Test;

import com.campsite.domain.dates.BookedDate;
import com.campsite.domain.exceptions.BookingDateIsNotBetween1And30DaysAheadOfArrivalDateException;
import com.campsite.domain.exceptions.WantToBookDatesAreNotConsecutiveException;

public class BookedDateTest {
    private Booking bookingService;

    @Before
    public void setup() {
        bookingService = new Booking();
        bookingService.setBookingId(BookingId.withBookingId(1));
    }

    @Test
    public void givenBookingDateIsOneDayAheadOfArrivalDateAndWantToBookDatesAre3ConsecutivesWhenBookingThenIGetABookingInPendingStatus() {
        final LocalDate now = now();
        final List<LocalDate> wantToBookDates = asList(now, now.plusDays(1), now.plusDays(2));
        final Booking booking = bookingService.newBookedDates(now.minusDays(1), wantToBookDates);
        assertEquals(new BookedDate(wantToBookDates.get(0)), booking.getBookedDates().get(0));
        assertEquals(new BookedDate(wantToBookDates.get(1)), booking.getBookedDates().get(1));
        assertEquals(new BookedDate(wantToBookDates.get(2)), booking.getBookedDates().get(2));
    }

    @Test(expected = BookingDateIsNotBetween1And30DaysAheadOfArrivalDateException.class)
    public void givenBookingDateIsSameAsArrivalDateWhenBookingThenIllegalArgumentBookingExceptionIsThrown() {
        final LocalDate now = now();
        List<LocalDate> wantToBookDates = asList(now, now.plusDays(1), now.plusDays(2));
        bookingService.newBookedDates(now, wantToBookDates);
    }

    @Test(expected = WantToBookDatesAreNotConsecutiveException.class)
    public void givenWantToBookDatesAreNotConsecutiveWhenBookingThenIllegalArgumentBookingExceptionIsThrown() {
        final LocalDate now = now();
        List<LocalDate> wantToBookDates = asList(now, now.plusDays(1), now.plusDays(3));
        bookingService.newBookedDates(now.minusDays(1), wantToBookDates);
    }

    @Test(expected = WantToBookMoreThan3DaysException.class)
    public void givenWantToBookDatesAreMoreThanThreeWhenBookingThenIllegalArgumentBookingExceptionIsThrown() {
        final LocalDate now = now();
        List<LocalDate> wantToBookDates = asList(now, now.plusDays(1), now.plusDays(3), now.plusDays(4));
        bookingService.newBookedDates(now.minusDays(1), wantToBookDates);
    }
}
