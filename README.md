Este proyecto se basa en 2 modulos diseniados segun DDD y desarrollado su dominio usando TDD.
Se propone un modulo que solo maneja la disponibilidad o availability de fechas y otro modulo que se encarga de 
gestionar las reservas o bookings.
Si bien el proyecto fue desarrollado para deployarse en un monolitico, es facilmente trasladable a un esquema de microservicos, 
mediante la inclusion de una cola o sistema de intercambio de mensajes en forma asyncronica request/reply entre ambos modulos.
Se utiliza una idea de SAGA, muy basica para realizar transacciones distribuidas sobre cada agregado.
Se utiliza offline optimistic locking en cada agregado para evitar inconsistent updates, tambien se realizan acciones de compensacion
como countermeasures.